Control panel for Feelelec wave generators connected by USB.
Tested with a FY6900-100 device, provides manual adjustment of all 
parameters. Also allows arbitrary waveform editing and uploading 
to the device through the USB interface.

The following functions are not implemented yet:

- Selected wave graphic display in the control panel
- Formula introduction help

