# This Python file uses the following encoding: utf-8
# Copyright © 2020 by Juanjo M. Santamarta. santamarta@gentlemansclub.de
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from PySide2.QtWidgets import (
    QApplication,
    QMainWindow,
    QFileDialog,
)
from PySide2.QtCore import (
    Slot,
    QTimer,
    QSemaphore,
    QCoreApplication,
    QObject,
    SIGNAL,
)

from uielements import Ui_feelman, formDict
# from PySide2 import QtCore, QtGui, QtUiTools, QtWidgets

import sys
import serial
import serial.tools.list_ports as list_ports
import logging
from functools import partial
import collections
import time
import modulation
import csv
import gzip

MAX_SAMPLES = 8192

def comp2(val, bits):
    """compute the 2's complement of int value val"""
    if (val & (1 << (bits - 1))) != 0:  # if sign bit is set e.g, 8bit: 128-255
        val = val - (1 << bits)  # compute negative value
    return val  #


class FeelElec(QMainWindow):
    def __init__(self):
        super(FeelElec, self).__init__()
        self.serTty = None
        self.portBusy = QSemaphore(1)
        self.semCommandQueue = QSemaphore(1)
        self.commandQueue = collections.deque({})
        self.modPar = None
        self.intervaloMedida = 1
        self.measureObject = 0
        self.waveData = [0.0] * MAX_SAMPLES
        self.chartview = None
        # Temporizador periodico para la medida
        self.tick = QTimer()
        self.setupControls()
        QObject.connect(
            self.tick, SIGNAL("timeout()"), self.readMeasurementState
        )
        QTimer.singleShot(500, self.enumeratePorts)

    def sendSequence(self, sequence):
        """Envía una secuencia al dispositivo y devuelve la respuesta
        sin bloquear el puerto, llamar con el puerto bloqueado
        """
        try:
            self.serTty.write(sequence)
            response = self.serTty.read_until().decode("utf-8", "ignore")
            return response
        except serial.SerialException:
            log.error("Serial port access error")
        except AttributeError:
            # Probablemente serTty es None, lo dejo para que
            # pueda probarlo quien no tenga el dispositivo
            log.debug("Exception sending command. Device not connected ?")

    def sendCommand(self, command):
        """ Envía un comando al dispositivo, bloqueando el puerto """
        self.portBusy.acquire()
        print(command)
        rsp = ""
        try:
            self.serTty.flushInput()
            rsp = self.sendSequence(bytes(command + "\n", "utf-8"))[
                :-1
            ]  # Elimino el \n final de la respuesta
        except serial.SerialException:
            log.error("Serial port access error")
        except AttributeError:
            log.debug("Exception sending command. Device not connected ?")
        self.portBusy.release()
        QCoreApplication.processEvents()
        return rsp

    def consumeCommandQueue(self):
        inqueue = True
        while inqueue:
            try:
                command = self.commandQueue.pop()
                self.sendCommand(command)
            except IndexError:
                inqueue = False
        self.semCommandQueue.release()

    def applyValues(self, values):
        """Lanza una serie de comandos hacia el
        dispositivo. Ignora las respuestas. Bloquea
        la cola, se libera al acabar"""
        self.semCommandQueue.acquire()
        for key, val in values.items():
            self.commandQueue.appendleft(key + " " + str(val))
        QTimer.singleShot(0, self.consumeCommandQueue)

    """
    def searchCommands(self):
        known_commands = ['SOB', 'SST', 'SEN', 'STI', 'SMO', 'SBE', 'SXY']
        for let1 in string.ascii_uppercase:
            start = 'S'+let1
            for let in string.ascii_uppercase:
                cmd = start + let
                if cmd not in known_commands:
                    cmd = cmd + ' 0'
                    print(cmd, end=' ')
                    resp = self.sendCommand(cmd)
                    if resp != 'Error':
                        print('-->'+resp)
                        return
                    time.sleep(0.1)
    """

    def enableChannel(self, channel, state):
        self.sendCommand("W" + channel + "N " + ("1" if state else "0"))
        time.sleep(0.1)

    def salir(self):
        app.quit()

    def updateView(self):
        if self.serTty is not None:
            self.readDeviceSetup()

    def readDeviceSetup(self):
        w = self.window
        if self.serTty is None:
            return False

        # Buzzer
        w.checkBox_beep.blockSignals(True)
        w.checkBox_beep.setChecked(self.sendCommand("UBZ") == "1")
        w.checkBox_beep.blockSignals(False)

        # Leer ajustes canal 1
        mwave = int(self.sendCommand("RMW"))
        if mwave > w.comboBox_wave_select_1.count():
            w.spinBox_arb_wave_select_1.setValue(
                mwave - w.comboBox_wave_select_1.count() + 2
            )
            mwave = w.comboBox_wave_select_1.count() - 1
        w.comboBox_wave_select_1.setCurrentIndex(mwave)
        w.dSBox_freq_select_1.setValue(float(self.sendCommand("RMF")))
        w.dSBox_amplitude_select_1.setValue(
            float(self.sendCommand("RMA")) / 10000
        )
        w.dSBox_duty_1.setValue(float(self.sendCommand("RMD")) / 1000)
        w.dSBox_offset_1.setValue(
            float(comp2(int(self.sendCommand("RMO")), 32)) / 1000
        )
        w.dSBox_phase_1.setValue(float(self.sendCommand("RMP")))
        w.dSBox_phase_1.setValue(float(self.sendCommand("RMP")) / 1000)
        # bloquear la emisión de señal cuando cambiamos el valor
        w.groupBox_c1.blockSignals(True)
        w.groupBox_c1.setChecked(self.sendCommand("RMN") == "255")
        w.groupBox_c1.blockSignals(False)

        # Lo mismo para el canal 2
        mwave = int(self.sendCommand("RFW"))
        if mwave >= w.comboBox_wave_select_2.count():
            w.spinBox_arb_wave_select_2.setValue(
                mwave - w.comboBox_wave_select_2.count() + 2
            )
            mwave = w.comboBox_wave_select_2.count() - 1
        w.comboBox_wave_select_2.setCurrentIndex(mwave)
        w.dSBox_freq_select_2.setValue(float(self.sendCommand("RFF")))
        w.dSBox_amplitude_select_2.setValue(
            float(self.sendCommand("RFA")) / 10000
        )
        w.dSBox_duty_2.setValue(float(self.sendCommand("RFD")) / 1000)
        w.dSBox_offset_2.setValue(
            float(comp2(int(self.sendCommand("RFO")), 32)) / 1000
        )
        w.dSBox_phase_2.setValue(float(self.sendCommand("RFP")) / 1000)
        # bloquear la emisión de señal cuando cambiamos el valor
        w.groupBox_c2.blockSignals(True)
        w.groupBox_c2.setChecked(self.sendCommand("RFN") == "255")
        w.groupBox_c2.blockSignals(False)

        # Leer modulación
        tipoMod = int(self.sendCommand("RPF"))
        entrMod = int("0" + self.sendCommand("RPM"))
        w.buttonGroup_Modulacion.button(tipoMod).setChecked(True)
        w.buttonGroup_OrigenModulacion.button(entrMod).setChecked(True)
        self.setModulationForm(tipoMod, entrMod)

        # Medidas
        tCta = int(self.sendCommand("RCG"))
        self.intervaloMedida = 10 ^ tCta
        w.buttonGroup_Medida_intervalo.blockSignals(True)
        w.buttonGroup_Medida_intervalo.button(tCta).setChecked(True)
        w.buttonGroup_Medida_intervalo.blockSignals(False)

        # Uplink
        w.groupBox_uplink.blockSignals(True)
        w.groupBox_uplink.setChecked(self.sendCommand("RUL") == "255")
        w.groupBox_uplink.blockSignals(False)
        # No lo uso por ahora
        w.comboBox_uplink_select_role.blockSignals(True)
        self.uplink_id = self.sendCommand("UID")
        w.comboBox_uplink_select_role.setCurrentIndex(
            int(self.sendCommand("RMS"))
        )
        w.comboBox_uplink_select_role.blockSignals(False)

        # FIXME: falta el acoplamiento ¿y el tipo de cuenta ?
        return True

        # Medidas
        # RCF:Read frequency of external measurement
        # Parece que lee la cuenta, pues varia con el tiempo de retencion
        # RCC:Read external counting value.
        # WCZ 0:Reset the counter.
        # WCP 0:Pause the measurement.
        # RCT:Read the external counting period.
        # RC+:Read width of positive pulse of external measurement.
        # RC-:Read width of negative pulse of external measurement.
        # RCD:Read the duty cycle of external measurement.
        # WCG x:Set the gate time of measurement. (0=1, 1=10, 2=100)
        # WCC:Set the coupling mode of measurement. (0=DC, 1=AC)

        #
        # FIXME: Faltan los de enlace y sistema
        #
        #

    def setMeasurementToZero(self):
        self.sendCommand("WCZ 0")
        self.readMeasurementState()

    def setMeasurementObject(self):
        self.measureObject = (
            self.window.buttonGroup_Medida_cuenta_freq.checkedId()
        )
        log.info("measureObject = " + str(self.measureObject))
        # self.window.buttonGroup_Medida_cuenta_freq.checkedId()

    def setMeasurementInterval(self):
        interval = self.window.buttonGroup_Medida_intervalo.checkedId()
        self.intervaloMedida = 10 ^ interval
        self.tick.stop()
        self.tick.start(self.intervaloMedida * 1000)
        self.sendCommand("WCG " + str(interval))

    def readMeasurementState(self):
        """ Lee los valores de medida externa """
        w = self.window
        if self.measureObject == 1:
            w.lineEdit_Measurement_1.setText(
                self.sendCommand("RCC")  # External counting value
            )
        else:
            w.lineEdit_Measurement_2.setText(
                self.sendCommand("RCF") + "Hz."  # Frequency
            )
        w.lineEdit_Measurement_3.setText(
            self.sendCommand("RC+") + "nS."  # width of positive pulse
        )
        w.lineEdit_Measurement_4.setText(
            self.sendCommand("RC-") + "nS."  # width of negative pulse
        )
        w.lineEdit_Measurement_5.setText(
            str(int(self.sendCommand("RCD")) / 10) + " %"  # duty cycle
        )

    def setMeasurementStateRev(self, state):
        self.setMeasurementState(not state)

    def setMeasurementState(self, state):
        """La medida es complicada de modelar... En el momento que se lanza
        una petición de medida, se arranca la medida independientemente de que
        estuviese en pausa. A ver qué saco"""
        if state:
            self.sendCommand("WCP 1")
            # Activado, leer los valores
            self.readMeasurementState()
            self.tick.start(self.intervaloMedida * 1000)
        else:
            self.tick.stop()
            self.sendCommand("WCP 0")
        log.debug("Estado: " + str(state))

    def changeMeasureCoupling(self, coupling):
        self.sendCommand("WCC " + str(coupling))

    def setModulationForm(self, tipoMod, entrMod):
        w = self.window
        lay = w.modulation_parameter.parent().layout()
        log.debug("Tipo modulación: " + str(tipoMod))

        if self.modPar is None or self.modPar.tipoMod != tipoMod:
            mp = modulation.modulationTypes(tipoMod)
            if mp.spin is None:
                w.label_modulationParameter.setText("Sin parámetros")
                w.modulation_parameter.clear()
                w.modulation_parameter.setDisabled(True)
                w.label_modulationParameter.setDisabled(True)
            else:
                w.modulation_parameter.setDisabled(False)
                w.label_modulationParameter.setDisabled(False)
                w.label_modulationParameter.setText(mp.p_dsc)
                # Leer valor externo, hemos cambiado de tipo
                mp.spin.setValue(
                    mp.conv_read(float(self.sendCommand("R" + mp.c_PM)))
                )
                lay.replaceWidget(w.modulation_parameter, mp.spin)
                w.modulation_parameter = mp.spin
                w.modulation_parameter.valueChanged.connect(
                    self.setModParDelayed
                )
                lay.update()
            if self.modPar is None or self.modPar.inputMap != mp.inputMap:
                if mp.inputMap == 0:  # 4 botones
                    w.pushButton_modulation_Ext_AC.setText("Ext.(AC)")
                    w.pushButton_modulation_Manual.setDisabled(False)
                    w.pushButton_modulation_Ext_DC.setDisabled(False)
                else:  # 2 botones
                    w.pushButton_modulation_Ext_AC.setText("Ext.(VCO In)")
                    w.pushButton_modulation_Manual.setDisabled(True)
                    w.pushButton_modulation_Ext_DC.setDisabled(True)

            self.modPar = mp

    def setSweepForm(self, obSw):
        # w = self.window
        pass

    # Barrido
    # SOB:Set the object of sweep. ( 0=frec, 1=ampl. 2=despl. 3=ciclo util )
    # SST:Set the start position of sweep.
    # SEN:Set the sweep end position.
    # STI:Set the sweep time
    # SMO:Set the sweep mode
    # SBE:Set the sweep on/off.
    # SXY:Set the control source of sweep.
    # SMD:Selecciona direccion

    def setSBFormat(self, sb, suffix, min, max, decimals, value):
        """ Asigna varias propiedades a un spinBox """
        sb.setSuffix(" " + suffix)
        sb.setMinimum(min)
        sb.setMaximum(max)
        if value is not None:
            sb.setValue(value)

    def setObjBarrido(self):
        """Selecciona el objeto del barrido: Frecuencia,
        amplitud, nivel CC o ciclo útil."""
        obSw = self.window.buttonGroup_Obj_Barrido.checkedId()
        self.sendCommand("SOB " + str(obSw))
        low = self.window.dSBox_sweep_low
        high = self.window.dSBox_sweep_high
        if obSw == 0:
            self.setSBFormat(low, "Hz", 0, 999999999.9, 1, 10000)
            self.setSBFormat(high, "Hz", 0, 999999999.9, 1, 20000)
        elif obSw == 1:
            self.setSBFormat(low, "V", 0, 12, 3, 0)
            self.setSBFormat(high, "V", 0, 12, 3, 5)
        elif obSw == 2:
            self.setSBFormat(low, "V", 0, 12, 3, 0)
            self.setSBFormat(high, "V", 0, 12, 3, 2)
        elif obSw == 3:
            self.setSBFormat(low, "%", 0, 100, 3, 50)
            self.setSBFormat(high, "%", 0, 100, 3, 80)

    def setTipoBarrido(self):
        """Selecciona entre barrido por VCO o por tiempo. El interfaz
        es idéntico aparte de esa selección"""
        tSw = self.window.buttonGroup_Tipo_Barrido.checkedId()
        self.sendCommand("SXY " + str(tSw))
        self.window.dSBox_sweep_time.setEnabled(tSw == 0)
        self.window.pushButton_sweep_back_forth.setEnabled(tSw == 0)

    def setSweepDirection(self):
        self.sendCommand(
            "SMD "
            + str(self.window.buttonGroup_Barrido_adelante_atras.checkedId())
        )

    def enableBeep(self):
        self.sendCommand(
            "UBZ " + ("1" if self.window.checkBox_beep.isChecked() else "0"),
        )

    def setSweepMode(self):
        self.sendCommand(
            "SMO "
            + str(self.window.buttonGroup_Barrido_Lineal_log.checkedId())
        )

    def applySweep(self):
        """ Envía los valores del formulario de barrido al aparato """
        w = self.window
        if w.dSBox_sweep_time.isEnabled():
            self.sendCommand("STI " + str(w.dSBox_sweep_time.value()))
        self.applyValues(
            {
                "SST": w.dSBox_sweep_low.value(),
                "SEN": w.dSBox_sweep_high.value(),
                "SMO": w.buttonGroup_Barrido_Lineal_log.checkedId(),
                "SMD": str(w.buttonGroup_Barrido_adelante_atras.checkedId()),
                "SBE": "1" if w.pushButton_sweep_start.isChecked() else "0",
            }
        )

    def readCounters(self):
        self.window.Measurement_1.setValue(float(self.sendCommand("RFF")))

    def detectDevice(self, portDev):
        if portDev.vid == 6790 and portDev.pid == 29987:
            self.serTty = serial.Serial(
                port=portDev.device,
                baudrate=115200,
                parity=serial.PARITY_NONE,
                stopbits=serial.STOPBITS_ONE,
                bytesize=serial.EIGHTBITS,
                timeout=2,  # Máxima espera de lectura
            )
            log.info("Succesfully opened: " + portDev.name)
            model = self.sendCommand("UMO")
            if model[0:6] == "FY6900":
                self.setWindowTitle(
                    "Function generator " + model + " en " + portDev.device
                )
                self.statusbar.showMessage(model + ":" + portDev.device)
                # log.debug(self.window.tabWidget.cornerWidget().text())
                return True
            else:
                self.serTty.close()
                self.serTty = None
                return False
        else:
            return False

    @Slot(str)
    def setCurrentPortDev(self, devPort):
        for aport in list(self.all_ports):
            if aport.device == devPort:
                self.setWindowTitle("Function generator. Trying " + devPort)
                if self.detectDevice(aport):
                    self.readDeviceSetup()

    def enumeratePorts(self):
        log.debug("Clearing menu")
        self.port_ActionEntries = list()
        self.window.menuPuerto_serie.clear()
        self.window.menuPuerto_serie.addAction(
            "Auto detectar", self.portAutoDetect
        )
        all_port_tuples = sorted(
            list_ports.comports(False), key=lambda device: device.name
        )
        self.all_ports = set()
        for aport in all_port_tuples:
            self.all_ports |= {aport}

        for aport in list(self.all_ports):
            action = self.window.menuPuerto_serie.addAction(aport.name)
            action.triggered.connect(
                partial(self.setCurrentPortDev, aport.device)
            )

    def portAutoDetect(self):
        self.enumeratePorts()
        for aport in list(self.all_ports):
            log.debug("trying port: " + str(aport.device))

            try:
                if self.detectDevice(aport):
                    self.readDeviceSetup()
            except (serial.SerialException):
                warn_str = "Tried to use port %s. Failed." % aport.device
                log.warning(warn_str)

    def checkWaveIndex1(self, cur_index):
        w = self.window
        if cur_index == self.window.comboBox_wave_select_1.count() - 1:
            w.spinBox_arb_wave_select_1.setDisabled(False)
        else:
            w.spinBox_arb_wave_select_1.setDisabled(True)

    def checkWaveIndex2(self, cur_index):
        w = self.window
        if cur_index == self.window.comboBox_wave_select_2.count() - 1:
            w.spinBox_arb_wave_select_2.setDisabled(False)
        else:
            w.spinBox_arb_wave_select_2.setDisabled(True)

    def applyChannel1State(self):
        w = self.window
        wave = w.comboBox_wave_select_1.currentIndex()
        if wave == w.comboBox_wave_select_1.count() - 1:
            wave = wave + w.spinBox_arb_wave_select_1.value() - 1
        self.applyValues(
            {
                "WMW": wave,
                "WMF": w.dSBox_freq_select_1.value(),
                "WMA": w.dSBox_amplitude_select_1.value(),
                "WMD": w.dSBox_duty_1.value(),
                "WMO": w.dSBox_offset_1.value(),
                "WMP": w.dSBox_phase_1.value(),
            }
        )

    def applyChannel2State(self):
        w = self.window
        wave = w.comboBox_wave_select_2.currentIndex()
        if wave == w.comboBox_wave_select_2.count() - 1:
            wave = wave + w.spinBox_arb_wave_select_2.value() - 1
        self.applyValues(
            {
                "WFW": wave,
                "WFF": w.dSBox_freq_select_2.value(),
                "WFA": w.dSBox_amplitude_select_2.value(),
                "WFD": w.dSBox_duty_2.value(),
                "WFO": w.dSBox_offset_2.value(),
                "WFP": w.dSBox_phase_2.value(),
            }
        )

    def setModParDelayed(self):
        QTimer.singleShot(200, self.setModulationParameter)

    def setModulationParameter(self):
        """Envia al aparato el parámetro de
        modulación"""
        w = self.window
        cmds = {}
        if self.modPar is not None:
            cmds["W" + self.modPar.c_PM] = str(
                self.modPar.conv_write(w.modulation_parameter.text())
            )
            self.applyValues(cmds)

    def load_settings(self):
        """ Lee los ajustes de un archivo,
        líneas de la forma 'etiqueta: valor'
        una línea por elemento
        """
        dict = formDict.getInstance()
        fileName = QFileDialog.getOpenFileName(
            self, "Abrir archivo", ".", "*.cfg"
        )
        if fileName[0]:
            with gzip.open(fileName[0], "rt") as file:
                lin = file.readline().split(": ")
                while len(lin) == 2:
                    control = dict[lin[0]]
                    if control is not None:
                        control.setStatus(lin[1].rstrip())
                    else:
                        print("Elemento desconocido: "+control)
                    lin = file.readline().split(": ")

    def save_settings(self):
        fileName = QFileDialog.getSaveFileName(
            self, "Guardar en...", ".", "*.cfg"
        )
        if fileName[0]:
            with gzip.open(fileName[0], "wt") as file:
                for dsc, elem in formDict.getInstance().items():
                    file.write(dsc+': '+elem.getStatus()+'\n')

    def applyExpression(self):
        """Responde al botón de la expresión, pero lo maneja el propio
        chartview"""
        self.chartview.applyExpression(self.window.lineEdit_Expression.text())

    def hex_dumpW(self, label, data):
        odd = False
        s = label
        b = None
        for a in data:
            if odd:
                s = s + "{:02X}{:02X} ".format(b, a)
            else:
                b = a
            odd = not odd
        print(s)

    def sendSequenceSlow(self, data, chunksize=32):
        """Envía los datos al dispositivo en trozos
        comprobando si existe respuesta entre trozo y trozo"""
        n = 0
        s = ""
        while n < len(data):
            chunk = data[n: chunksize + n]
            # self.hex_dumpW(f'{n//2:04d}: ' ,chunk)
            s = s + str(self.sendSequence(chunk))
            n = n + chunksize
            if len(s) > 0:
                break
        s = s + str(self.sendSequence(b""))
        return s

    def loadWaveData(self):
        fileName = QFileDialog.getOpenFileName(
            self, "Abrir archivo", ".", "*.csv"
        )
        if fileName[0]:
            with open(fileName[0]) as csvfile:
                reader = csv.reader(csvfile)
                for index, row in enumerate(reader):
                    self.waveData[index] = float(row[0])
                    if index >= MAX_SAMPLES:
                        break
                for i in range(index + 1, MAX_SAMPLES):
                    self.waveData[i] = 0.0
                self.chartview.replaceData(self.waveData)

    def saveWaveData(self):
        self.chartview.getValuesFromGraph()  # Tiene una referencia a waveData
        fileName = QFileDialog.getSaveFileName(
            self, "Guardar en...", ".", "*.csv"
        )
        if fileName[0]:
            with open(fileName[0], "w") as csvfile:
                writer = csv.writer(csvfile)
                for index, field in enumerate(self.waveData):
                    writer.writerow([self.waveData[index]])

    def sendWaveData(self):
        """Envía la onda a la memoria indicada en el generador
        FIXME: Comprobar si la onda está en uso
        """
        rdata = []
        mem = self.window.spinBox_wave_mem.value()
        self.chartview.getValuesFromGraph()  # Tiene una referencia a waveData
        # for x, y in enumerate(self.waveData):
        #     print(x, y)
        # El dispositivo no acepta ningún comando hasta que termine,
        # mantenerlo bloqueado hasta acabar
        self.portBusy.acquire()
        self.serTty.timeout = 2
        self.serTty.flushInput()
        rsp = self.sendSequence(b"DDS_WAVE%u\n" % mem)
        if rsp[0] == "W":
            # print(rsp)
            vmin = min(self.waveData)
            vmax = max(self.waveData)
            print(f"vmin: {vmin} vmax: {vmax}")
            # Este ajuste elimina el offset y ajusta la señal
            for x, y in enumerate(self.waveData):
                v = (int((y - vmin) * (2 ** 14 - 1) / (vmax - vmin))
                     if vmax > vmin else int(0)
                     )
                rdata.extend(v.to_bytes(2, byteorder="big"))
            self.serTty.timeout = 0.01
            rsp = self.sendSequenceSlow(bytes(rdata))
            # FIXME: Tratar correctamente la finalización
            # y dar un error si la respuesta no es la esperada
            # print(f"Respuesta onda:{rsp}") # debe ser 'H'
            self.serTty.timeout = 2
            # Esperar por la indicación de grabación
            rsp = self.sendSequence(b"")
            # print(f"Respuesta posterior:{rsp}") # debe ser 'N\n'
            self.serTty.timeout = 2
        else:
            # FIXME: error, indicarlo en un dialog
            log.error(
                f"Error: {rsp}, el dispositivo puede haber"
                " quedado bloqueado, apáguelo"
            )
        self.portBusy.release()

    def setModulacion(self):
        """Se dispara con los cambios en los controles del groupbox de
        modulacion, directamente con los botones y con un timeout en el
        campo de entrada de datos. Enviar los comandos al aparato"""
        """ FIXME: Hay un comando de modulación que no sé qué hace
        # (no tiene comando de lectura RPO)
        # WPO: Generating Manual Sources Each time the signal generator
        # receives the instruction, it generates a manual source.
        # Quizás haya que lanzarlo al seleccionar modulacion manual """
        w = self.window
        tipoMod = w.buttonGroup_Modulacion.checkedId()
        entrMod = w.buttonGroup_OrigenModulacion.checkedId()
        self.setModulationForm(tipoMod, entrMod)
        cmds = {"WPF": str(tipoMod), "WPM": str(entrMod)}
        log.debug(self.window.modulation_parameter.text())
        if self.modPar is not None and self.modPar.c_PM is not None:
            cmds["W" + self.modPar.c_PM] = str(
                self.modPar.conv_write(w.modulation_parameter.text())
            )
        log.debug(cmds)
        self.applyValues(cmds)

    def setupControls(self):
        """Inicializa los controles y establece sus eventos
        y valores iniciales. """

        log.debug("SetupControls")
        self.resize(1310, 800)
        ui = Ui_feelman()
        ui.setupUi(self)
        self.window = self
        w = self
        w.comboBox_wave_select_1.currentIndexChanged.connect(
            self.checkWaveIndex1
        )
        w.comboBox_wave_select_2.currentIndexChanged.connect(
            self.checkWaveIndex2
        )
        w.buttonGroup_Modulacion.buttonClicked.connect(self.setModulacion)
        w.buttonGroup_OrigenModulacion.buttonClicked.connect(
            self.setModulacion
        )
        w.actionAuto_detectar.triggered.connect(self.portAutoDetect)
        w.actionSalir.triggered.connect(self.salir)

        w.checkBox_beep.toggled.connect(self.enableBeep)

        w.groupBox_c1.toggled.connect(partial(self.enableChannel, "M"))
        w.groupBox_c2.toggled.connect(partial(self.enableChannel, "F"))

        w.pushButton_apply_1.pressed.connect(self.applyChannel1State)
        w.pushButton_apply_2.pressed.connect(self.applyChannel2State)

        w.buttonGroup_Modulacion.setId(w.pushButton_modulation_FSK, 0)
        w.buttonGroup_Modulacion.setId(w.pushButton_modulation_ASK, 1)
        w.buttonGroup_Modulacion.setId(w.pushButton_modulation_PSK, 2)
        w.buttonGroup_Modulacion.setId(w.pushButton_modulation_Burst, 3)
        w.buttonGroup_Modulacion.setId(w.pushButton_modulation_AM, 4)
        w.buttonGroup_Modulacion.setId(w.pushButton_modulation_FM, 5)
        w.buttonGroup_Modulacion.setId(w.pushButton_modulation_PM, 6)
        w.buttonGroup_OrigenModulacion.setId(
            w.pushButton_modulation_Channel2, 0
        )
        w.buttonGroup_OrigenModulacion.setId(w.pushButton_modulation_Ext_AC, 1)
        w.buttonGroup_OrigenModulacion.setId(w.pushButton_modulation_Manual, 2)
        w.buttonGroup_OrigenModulacion.setId(w.pushButton_modulation_Ext_DC, 3)
        w.buttonGroup_Tipo_Barrido.setId(w.pushButton_sweep_Time, 0)
        w.buttonGroup_Tipo_Barrido.setId(w.pushButton_sweep_VCO, 1)
        w.buttonGroup_Obj_Barrido.setId(w.pushButton_sweep_frequency, 0)
        w.buttonGroup_Obj_Barrido.setId(w.pushButton_sweep_amplitude, 1)
        w.buttonGroup_Obj_Barrido.setId(w.pushButton_sweep_offset, 2)
        w.buttonGroup_Obj_Barrido.setId(w.pushButton_sweep_duty, 3)
        w.buttonGroup_Tipo_Barrido.buttonClicked.connect(self.setTipoBarrido)
        w.buttonGroup_Obj_Barrido.buttonClicked.connect(self.setObjBarrido)
        w.buttonGroup_Barrido_Lineal_log.buttonClicked.connect(
            self.setSweepMode
        )
        w.buttonGroup_Barrido_Lineal_log.setId(w.pushButton_sweep_linear, 0)
        w.buttonGroup_Barrido_Lineal_log.setId(
            w.pushButton_sweep_logaritmic, 1
        )
        w.buttonGroup_Barrido_adelante_atras.buttonClicked.connect(
            self.setSweepDirection
            # self.searchCommands
        )
        w.buttonGroup_Barrido_adelante_atras.setId(w.pushButton_sweep_forth, 0)
        w.buttonGroup_Barrido_adelante_atras.setId(w.pushButton_sweep_back, 1)
        w.buttonGroup_Barrido_adelante_atras.setId(
            w.pushButton_sweep_back_forth, 2
        )
        w.pushButton_sweep_start.clicked.connect(self.applySweep)
        w.buttonGroup_Medida_intervalo.setId(w.pushButton_measurement_1s, 0)
        w.buttonGroup_Medida_intervalo.setId(w.pushButton_measurement_10s, 1)
        w.buttonGroup_Medida_intervalo.setId(w.pushButton_measurement_100s, 2)
        w.groupBox_meter.toggled.connect(self.setMeasurementState)
        w.buttonGroup_Medida_intervalo.buttonClicked.connect(
            self.setMeasurementInterval
        )
        w.comboBox_Measurement_Coupling.currentIndexChanged.connect(
            self.changeMeasureCoupling
        )
        w.buttonGroup_Medida_cuenta_freq.setId(
            w.pushButton_Measurement_Freq, 0
        )
        w.buttonGroup_Medida_cuenta_freq.setId(
            w.pushButton_Measurement_Count, 1
        )
        w.buttonGroup_Medida_cuenta_freq.buttonClicked.connect(
            self.setMeasurementObject
        )
        w.pushButton_Measurement_Zero.pressed.connect(
            self.setMeasurementToZero
        )
        w.pushButton_Measurement_Stop.toggled.connect(
            self.setMeasurementStateRev
        )
        w.pushButton_save_settings.pressed.connect(self.save_settings)
        w.pushButton_load_settings.pressed.connect(self.load_settings)

        for i in range(8192):
            self.waveData[i] = 0  # random.uniform(-0.5, 0.5)
        # lay = QVBoxLayout(w.chart_box)
        # lay.setContentsMargins(0, 0, 0, 0)
        # lay = w.verticalLayout_tab_2
        # lay = w.chart_box
        # self.chartview = waveEdit.waveEdit(self.waveData)
        # lay.addWidget(self.chartview)
        w.BtnExpApply.pressed.connect(self.applyExpression)
        w.pushButton_Enviar.pressed.connect(self.sendWaveData)
        w.pushButton_Exportar.pressed.connect(self.saveWaveData)
        w.pushButton_Importar.pressed.connect(self.loadWaveData)


if __name__ == "__main__":
    log = logging.getLogger(__name__)
    logging.basicConfig()
    logging.getLogger().setLevel(logging.ERROR)
    # logging.getLogger().setLevel(logging.DEBUG)
    app = QApplication(sys.argv)
    iu = FeelElec()
    iu.show()
    sys.exit(app.exec_())
