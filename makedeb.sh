#!/bin/dash
if [ -d feelman ]
then rm feelman/*
else mkdir feelman
fi
cp __init__.py feelman
cp feelman.py feelman
cp feelman.desktop feelman
cp gpl-3.0.txt feelman
cp modulation.py feelman
cp LICENSE feelman
cp README.md feelman
cp waveEdit.py feelman
cp feelman.py feelman
cp uielements.py feelman

rm -rf deb_dist
rm -rf dist
rm -rf feelman.egg-info

python3 setup.py sdist
python3 setup.py --command-packages=stdeb.command sdist_dsc
python3 setup.py --command-packages=stdeb.command bdist_deb

