# This Python file uses the following encoding: utf-8

# Copyright © 2020 by Juanjo M. Santamarta. santamarta@gentlemansclub.de
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from PySide2.QtWidgets import QSpinBox, QDoubleSpinBox
from PySide2.QtCore import Qt
from uielements import savingSpinBox, savingDoubleSpinBox, savingLineEdit

def identity(x):
    return x

class modulationTypes:
    """Esta función devuelve un control y algunos parámetros
    para reemplazar el campo de entrada del parámetro cuando se
    cambia el tipo de modulación"""

    spin = None
    c_PM = None
    p_dsc = ""
    tipoMod = -1
    # Hay dos disposiciones de botones de entradas, indicar cual
    inputMap = 0

    def __init__(self, tipoMod):
        self.conv_read = identity
        self.conv_write = identity
        self.tipoMod = tipoMod
        if tipoMod == 0:  # FSK
            self.spin = savingDoubleSpinBox(
                parentWin = None,
                id = "mod_2freq",
                decimals=3,
                minimum = 0,
                maximum = 100000000,
                suffix = "Hz",
                alignment = Qt.AlignRight,
            )
            self.p_dsc = "Segunda frecuencia"
            self.c_PM = "FK"
        elif tipoMod == 1:  # ASK
            self.spin = None
            self.c_PM = None
        elif tipoMod == 2:  # PSK
            self.spin = None
            self.c_PM = None
        elif tipoMod == 3:  # Ráfaga
            self.spin = savingSpinBox(
                parentWin = None,
                id="mod_nPulsos",
                minimum=0,
                maximum=100000000,
                alignment=Qt.AlignRight,
            )
            self.c_PM = "PN"
            self.p_dsc = "Número de pulsos"
        elif tipoMod == 4:  # AM
            self.spin = savingDoubleSpinBox(
                parentWin = None,
                id = "mod_ratio",
                decimals = 3,
                minimum = 0,
                maximum = 100.000,
                suffix = "%",
                alignment=Qt.AlignRight,
            )
            self.c_PM = "PR"
            self.p_dsc = "Ratio de modulación"
            self.conv_read = lambda x: x / 10
            self.inputMap = 1
        elif tipoMod == 5:  # FM
            self.spin = savingDoubleSpinBox(
                parentWin = None,
                id="mod_recorrido_freq",
                decimals=3,
                minimum=0,
                maximum=100000000,
                suffix="Hz",
                alignment=Qt.AlignRight,
            )
            self.c_PM = "FM"
            self.p_dsc = "Recorrido de frecuencia"
            self.inputMap = 1
        elif tipoMod == 6:  # PM
            self.spin = QDoubleSpinBox(
                parentWin = None,
                id="mod_recorrido_fase",
                decimals=2,
                minimum=0,
                maximum=359.99,
                suffix="º",
                alignment=Qt.AlignRight,
            )
            self.c_PM = "PP"
            self.p_dsc = "Recorrido de fase"
            self.conv_read = lambda x: x / 100
            self.inputMap = 1
