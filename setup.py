# This Python file uses the following encoding: utf-8

import os
from setuptools import setup

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setup(
    name= "feelman",
    version= "0.75",
    author= "Juanjo Santamarta",
    author_email= "santamarta@bonitavista.es",
    description= "Control panel for FeelElec wave generators",
    long_description=long_description,
    long_description_content_type="text/markdown",
    license= "GPLv3",
    url= "",
    packages=['feelman'],
    entry_points = {
        'gui_scripts': ['feelman=feelman:main']
    },
    install_requires = [
        'serial',
        'pyside2.qtcharts',
        'pyside2.qtgui',
        'pyside2.qtuitools',
        'pyside2.qtwidgets',
    ],
    data_files= [
        ('share/applications/', ['feelman.desktop'])
    ],
    python_requires='>=3.6',
    classifiers= [
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GPLv3 License",
        "Operating System :: OS Independent",
    ],
)
