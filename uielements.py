# -*- coding: utf-8 -*-

# Generado automáticamente desde el .ui, y editado después ampliamente

from PySide2 import QtCore, QtWidgets
import waveEdit

waves1 = (
    "Senoidal", "Cuadrada", "Rectangular", "Trapezoide", "CMOS",
    "Pulso ajustable", "Continua", "Triangular", "Ascendente",
    "Descendente", "Escalera triangular", "Escalera ascendente",
    "Escalera descendente", "Exponencial positivo", "Exponencial negativo",
    "Caída exponencial positivo", "Caída exponencial negativo",
    "Logarítmica positiva", "Logarítmica negativa",
    "Caída logarítmica positiva", "Caída logarítmica negativa",
    "Rectificada positiva doble onda", "Rectificada negativa doble onda",
    "Rectificada positiva media onda", "Rectificada negativa media onda",
    "Pulso Lorentz", "Mutitono", "Ruido aleatorio", "Electrocardiograma",
    "Trapezoide", "Pulso seno cardinal", "impulso",
    "Ruido gaussiano aditivo", "Modulación amplitud",
    "Modulación frecuencia", "Frecuencia modulada pulsada", "Arbitraria"
    )

waves2 = (
    "Senoidal", "Cuadrada", "Rectangular", "Trapezoide", "CMOS", "Continua",
    "Triangular", "Ascendente", "Descendente", "Escalera triangular",
    "Escalera ascendente", "Escalera descendente", "Exponencial positivo",
    "Exponencial negativo", "Caída exponencial positivo",
    "Caída exponencial negativo", "Logarítmica positiva",
    "Logarítmica negativa", "Caída logarítmica positiva",
    "Caída logarítmica negativa", "Rectificada positiva doble onda",
    "Rectificada negativa doble onda", "Rectificada positiva media onda",
    "Rectificada negativa media onda", "Pulso Lorentz", "Mutitono",
    "Ruido aleatorio", "Electrocardiograma", "Trapezoide",
    "Pulso seno cardinal", "impulso", "Ruido gaussiano aditivo",
    "Modulación amplitud", "Modulación frecuencia",
    "Frecuencia modulada pulsada", "Arbitraria"
    )

exp = QtWidgets.QSizePolicy(
                        QtWidgets.QSizePolicy.Expanding,
                        QtWidgets.QSizePolicy.Expanding
                            )


class formDict(dict):
    """ Clase Singleton que proporciona un punto de acceso global
    para los elementos a salvar """
    __instance = None

    @staticmethod
    def getInstance():
        """ Método de acceso estático. """
        if formDict.__instance is None:
            formDict()
        return formDict.__instance

    def __init__(self):
        """ Virtually private constructor. """
        if formDict.__instance is not None:
            raise Exception("Esta clase solo puede instanciarse una vez")
        else:
            formDict.__instance = self


class savingControl:
    """ Los controles que desciendan de esta clase requieren un hash
    global llamado 'formDict'  donde se almacena el identificador
    del control y una referencia al mismo. Aquí lo implemento
    como un singleton
    """
    def __init__(self, id):
        formDict.getInstance()[id] = self

    def getStatus(self):
        pass

    def setStatus(self, data):
        pass


class savingWaveEdit(waveEdit.waveEdit, savingControl):
    def __init__(self, data, id):
        savingControl.__init__(self, id)
        waveEdit.waveEdit.__init__(self, data)

    def getStatus(self):
        # Quizas deberia reducir los valores a enteros
        # pero habría que normalizarlos
        return ' '.join([str(i) for i in self.getDataFromGraph()])

    def setStatus(self, data):
        self.replaceData([float(i) for i in data.split()])


class savingButtonGroup(QtWidgets.QButtonGroup, savingControl):
    def __init__(self, parentWin, id):
        savingControl.__init__(self, id)
        QtWidgets.QButtonGroup.__init__(self, parentWin)

    def getStatus(self):
        return str(self.checkedId())

    def setStatus(self, data):
        # FIXME: Esto no va
        for i in self.buttons():
            if self.id(i) == data:
                i.setChecked(True)
            else:
                i.setChecked(False)


class savingPushButton(QtWidgets.QPushButton, savingControl):
    # Para botones cambiables
    def __init__(self, parentWin, id):
        savingControl.__init__(self, id)
        QtWidgets.QPushButton.__init__(self, parentWin)

    def getStatus(self):
        return 'on' if self.isChecked() else 'off'

    def setStatus(self, data):
        self.setChecked(data == 'on')


class savingSpinBox(QtWidgets.QSpinBox, savingControl):
    def __init__(self, parentWin, id, **kwargs):
        savingControl.__init__(self, id)
        QtWidgets.QSpinBox.__init__(self, parentWin, **kwargs)

    def getStatus(self):
        return str(self.value())

    def setStatus(self, data):
        self.setValue(int(data))


class savingDoubleSpinBox(QtWidgets.QDoubleSpinBox, savingControl):
    def __init__(self, parentWin, id, **kwargs):
        savingControl.__init__(self, id)
        QtWidgets.QDoubleSpinBox.__init__(self, parentWin, **kwargs)

    def getStatus(self):
        return str(self.value())

    def setStatus(self, data):
        self.setValue(float(data))


class savingCheckBox(QtWidgets.QCheckBox, savingControl):
    def __init__(self, parentWin, id, **kwargs):
        savingControl.__init__(self, id)
        QtWidgets.QCheckBox.__init__(self, parentWin, **kwargs)

    def getStatus(self):
        return 'on' if self.isChecked() else 'off'

    def setStatus(self, data):
        self.setCheckState(data == 'on')


class savingGroupBox(QtWidgets.QGroupBox, savingControl):
    def __init__(self, parentWin, id, **kwargs):
        savingControl.__init__(self, id)
        QtWidgets.QGroupBox.__init__(self, parentWin, **kwargs)

    def getStatus(self):
        return 'on' if self.isChecked() else 'off'

    def setStatus(self, data):
        self.setChecked(data == 'on')


class savingLineEdit(QtWidgets.QLineEdit, savingControl):
    def __init__(self, parentWin, id, **kwargs):
        savingControl.__init__(self, id)
        QtWidgets.QLineEdit.__init__(self, parentWin, **kwargs)

    def getStatus(self):
        return self.text()

    def setStatus(self, data):
        self.setText(data)


class Ui_feelman(object):
    def setupUi(self, ap):
        ap.tabWidget = QtWidgets.QTabWidget(ap)
        ap.tab = QtWidgets.QWidget()
        ap.verticalLayout_global = QtWidgets.QVBoxLayout(ap.tab)
        ap.verticalLayout_global.setContentsMargins(0, 0, 0, 0)
        ap.horizontalLayout_5 = QtWidgets.QHBoxLayout()
        # ap.horizontalLayout_5.setSizeConstraint(QtWidgets.QLayout.SetMaximumSize)
        # Canal 1
        ap.groupBox_c1 = savingGroupBox(ap.tab, 'group_c1')
        ap.groupBox_c1.setCheckable(True)
        ap.groupBox_c1.setChecked(False)
        ap.gridLayout = QtWidgets.QGridLayout(ap.groupBox_c1)
        ap.verticalLayout_4 = QtWidgets.QVBoxLayout()
        ap.label_14 = QtWidgets.QLabel(ap.groupBox_c1)
        ap.verticalLayout_4.addWidget(ap.label_14, 0, QtCore.Qt.AlignBottom)
        ap.dSBox_phase_1 = savingDoubleSpinBox(ap.groupBox_c1, "phase_c1")
        ap.dSBox_phase_1.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        ap.dSBox_phase_1.setDecimals(3)
        ap.verticalLayout_4.addWidget(ap.dSBox_phase_1)
        ap.gridLayout.addLayout(ap.verticalLayout_4, 3, 0, 1, 1)
        ap.horizontalLayout = QtWidgets.QHBoxLayout()
        ap.label_4 = QtWidgets.QLabel(ap.groupBox_c1)
        ap.label_4.setText("")
        ap.horizontalLayout.addWidget(ap.label_4)
        ap.comboBox_wave_select_1 = QtWidgets.QComboBox(ap.groupBox_c1)
        for i in range(len(waves1)):
            ap.comboBox_wave_select_1.addItem("")
        ap.horizontalLayout.addWidget(ap.comboBox_wave_select_1)
        ap.spinBox_arb_wave_select_1 = savingSpinBox(ap.groupBox_c1, "wave_sel_c1")
        ap.spinBox_arb_wave_select_1.setEnabled(False)
        ap.spinBox_arb_wave_select_1.setMinimum(1)
        ap.spinBox_arb_wave_select_1.setMaximum(64)
        ap.horizontalLayout.addWidget(ap.spinBox_arb_wave_select_1)
        ap.gridLayout.addLayout(ap.horizontalLayout, 0, 0, 1, 2)
        ap.verticalLayout = QtWidgets.QVBoxLayout()
        ap.label_12 = QtWidgets.QLabel(ap.groupBox_c1)
        ap.verticalLayout.addWidget(ap.label_12, 0, QtCore.Qt.AlignBottom)
        ap.dSBox_amplitude_select_1 = savingDoubleSpinBox(ap.groupBox_c1,"ampl_c1")
        ap.dSBox_amplitude_select_1.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        ap.dSBox_amplitude_select_1.setDecimals(3)
        ap.dSBox_amplitude_select_1.setMinimum(0.0)
        ap.dSBox_amplitude_select_1.setMaximum(12.0)
        ap.dSBox_amplitude_select_1.setSingleStep(0.01)
        ap.dSBox_amplitude_select_1.setStepType(QtWidgets.QAbstractSpinBox.AdaptiveDecimalStepType)
        ap.dSBox_amplitude_select_1.setProperty("value", 0.0)
        ap.verticalLayout.addWidget(ap.dSBox_amplitude_select_1)
        ap.gridLayout.addLayout(ap.verticalLayout, 2, 0, 1, 1)
        ap.graphicsView_ch_1 = QtWidgets.QGraphicsView(ap.groupBox_c1)
        ap.graphicsView_ch_1.setMinimumSize(QtCore.QSize(120, 0))
        ap.gridLayout.addWidget(ap.graphicsView_ch_1, 0, 2, 3, 1)
        ap.pushButton_apply_1 = QtWidgets.QPushButton(ap.groupBox_c1)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        ap.pushButton_apply_1.setSizePolicy(sizePolicy)
        ap.gridLayout.addWidget(ap.pushButton_apply_1, 3, 2, 1, 1)
        ap.verticalLayout_5 = QtWidgets.QVBoxLayout()
        ap.label_15 = QtWidgets.QLabel(ap.groupBox_c1)
        ap.verticalLayout_5.addWidget(ap.label_15, 0, QtCore.Qt.AlignBottom)
        ap.dSBox_duty_1 = savingDoubleSpinBox(ap.groupBox_c1, "duty_c1")
        ap.dSBox_duty_1.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        ap.dSBox_duty_1.setDecimals(3)
        ap.dSBox_duty_1.setMaximum(100.0)
        ap.verticalLayout_5.addWidget(ap.dSBox_duty_1)
        ap.gridLayout.addLayout(ap.verticalLayout_5, 3, 1, 1, 1)
        ap.verticalLayout_2 = QtWidgets.QVBoxLayout()
        ap.label_13 = QtWidgets.QLabel(ap.groupBox_c1)
        ap.verticalLayout_2.addWidget(ap.label_13, 0, QtCore.Qt.AlignBottom)
        ap.dSBox_offset_1 = savingDoubleSpinBox(ap.groupBox_c1, "offset_c1")
        ap.dSBox_offset_1.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        ap.dSBox_offset_1.setMinimum(-12.0)
        ap.dSBox_offset_1.setMaximum(12.0)
        ap.dSBox_offset_1.setSingleStep(0.01)
        ap.dSBox_offset_1.setStepType(QtWidgets.QAbstractSpinBox.AdaptiveDecimalStepType)
        ap.dSBox_offset_1.setProperty("value", 0.0)
        ap.verticalLayout_2.addWidget(ap.dSBox_offset_1)
        ap.gridLayout.addLayout(ap.verticalLayout_2, 2, 1, 1, 1)
        ap.verticalLayout_3 = QtWidgets.QVBoxLayout()
        ap.label_11 = QtWidgets.QLabel(ap.groupBox_c1)
        ap.verticalLayout_3.addWidget(ap.label_11, 0, QtCore.Qt.AlignBottom)
        ap.dSBox_freq_select_1 = savingDoubleSpinBox(ap.groupBox_c1, "freq_c1")
        ap.dSBox_freq_select_1.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        ap.dSBox_freq_select_1.setDecimals(3)
        ap.dSBox_freq_select_1.setMaximum(100000000.0)
        ap.dSBox_freq_select_1.setStepType(QtWidgets.QAbstractSpinBox.AdaptiveDecimalStepType)
        ap.dSBox_freq_select_1.setProperty("value", 0.001)
        ap.verticalLayout_3.addWidget(ap.dSBox_freq_select_1)
        ap.gridLayout.addLayout(ap.verticalLayout_3, 1, 0, 1, 2)
        ap.horizontalLayout_5.addWidget(ap.groupBox_c1)
        # Canal 2
        ap.groupBox_c2 =savingGroupBox(ap.tab, "group_c2")
        ap.groupBox_c2.setCheckable(True)
        ap.groupBox_c2.setChecked(False)
        ap.gridLayout_5 = QtWidgets.QGridLayout(ap.groupBox_c2)
        ap.verticalLayout_17 = QtWidgets.QVBoxLayout()
        ap.label_27 = QtWidgets.QLabel(ap.groupBox_c2)
        ap.verticalLayout_17.addWidget(ap.label_27, 0, QtCore.Qt.AlignBottom)
        ap.dSBox_amplitude_select_2 = savingDoubleSpinBox(ap.groupBox_c2, "ampl_c2")
        ap.dSBox_amplitude_select_2.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        ap.dSBox_amplitude_select_2.setDecimals(3)
        ap.dSBox_amplitude_select_2.setMinimum(0.0)
        ap.dSBox_amplitude_select_2.setMaximum(12.0)
        ap.dSBox_amplitude_select_2.setSingleStep(0.01)
        ap.dSBox_amplitude_select_2.setStepType(QtWidgets.QAbstractSpinBox.AdaptiveDecimalStepType)
        ap.dSBox_amplitude_select_2.setProperty("value", 0.0)
        ap.verticalLayout_17.addWidget(ap.dSBox_amplitude_select_2)
        ap.gridLayout_5.addLayout(ap.verticalLayout_17, 2, 0, 1, 1)
        ap.verticalLayout_19 = QtWidgets.QVBoxLayout()
        ap.label_29 = QtWidgets.QLabel(ap.groupBox_c2)
        ap.verticalLayout_19.addWidget(ap.label_29, 0, QtCore.Qt.AlignBottom)
        ap.dSBox_phase_2 = savingDoubleSpinBox(ap.groupBox_c2, "phase_c2")
        ap.dSBox_phase_2.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        ap.dSBox_phase_2.setDecimals(3)
        ap.verticalLayout_19.addWidget(ap.dSBox_phase_2)
        ap.gridLayout_5.addLayout(ap.verticalLayout_19, 3, 0, 1, 1)
        ap.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        ap.comboBox_wave_select_2 = QtWidgets.QComboBox(ap.groupBox_c2)
        for i in range(len(waves2)):
            ap.comboBox_wave_select_2.addItem("")
        ap.horizontalLayout_4.addWidget(ap.comboBox_wave_select_2)
        ap.spinBox_arb_wave_select_2 = savingSpinBox(ap.groupBox_c2, "wave_sel_c2")
        ap.spinBox_arb_wave_select_2.setEnabled(False)
        ap.spinBox_arb_wave_select_2.setMinimum(1)
        ap.spinBox_arb_wave_select_2.setMaximum(64)
        ap.horizontalLayout_4.addWidget(ap.spinBox_arb_wave_select_2)
        ap.gridLayout_5.addLayout(ap.horizontalLayout_4, 0, 0, 1, 2)
        ap.pushButton_apply_2 = QtWidgets.QPushButton(ap.groupBox_c2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        ap.pushButton_apply_2.setSizePolicy(sizePolicy)
        ap.gridLayout_5.addWidget(ap.pushButton_apply_2, 3, 2, 1, 1)
        ap.graphicsView_ch_2 = QtWidgets.QGraphicsView(ap.groupBox_c2)
        ap.graphicsView_ch_2.setMinimumSize(QtCore.QSize(120, 0))
        ap.gridLayout_5.addWidget(ap.graphicsView_ch_2, 0, 2, 3, 1)
        ap.verticalLayout_20 = QtWidgets.QVBoxLayout()
        ap.label_30 = QtWidgets.QLabel(ap.groupBox_c2)
        ap.verticalLayout_20.addWidget(ap.label_30, 0, QtCore.Qt.AlignBottom)
        ap.dSBox_duty_2 = savingDoubleSpinBox(ap.groupBox_c2, "duty_c2")
        ap.dSBox_duty_2.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        ap.dSBox_duty_2.setDecimals(3)
        ap.dSBox_duty_2.setMaximum(100.0)
        ap.verticalLayout_20.addWidget(ap.dSBox_duty_2)
        ap.gridLayout_5.addLayout(ap.verticalLayout_20, 3, 1, 1, 1)
        ap.verticalLayout_18 = QtWidgets.QVBoxLayout()
        ap.label_28 = QtWidgets.QLabel(ap.groupBox_c2)
        ap.verticalLayout_18.addWidget(ap.label_28, 0, QtCore.Qt.AlignBottom)
        ap.dSBox_offset_2 = savingDoubleSpinBox(ap.groupBox_c2, "offset_c2")
        ap.dSBox_offset_2.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        ap.dSBox_offset_2.setMinimum(-12.0)
        ap.dSBox_offset_2.setMaximum(12.0)
        ap.dSBox_offset_2.setSingleStep(0.01)
        ap.dSBox_offset_2.setStepType(QtWidgets.QAbstractSpinBox.AdaptiveDecimalStepType)
        ap.dSBox_offset_2.setProperty("value", 0.0)
        ap.verticalLayout_18.addWidget(ap.dSBox_offset_2)
        ap.gridLayout_5.addLayout(ap.verticalLayout_18, 2, 1, 1, 1)
        ap.verticalLayout_16 = QtWidgets.QVBoxLayout()
        ap.label_26 = QtWidgets.QLabel(ap.groupBox_c2)
        ap.verticalLayout_16.addWidget(ap.label_26, 0, QtCore.Qt.AlignBottom)
        ap.dSBox_freq_select_2 = savingDoubleSpinBox(ap.groupBox_c2, "freq_c2")
        ap.dSBox_freq_select_2.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        ap.dSBox_freq_select_2.setDecimals(3)
        ap.dSBox_freq_select_2.setMaximum(100000000.0)
        ap.dSBox_freq_select_2.setStepType(QtWidgets.QAbstractSpinBox.AdaptiveDecimalStepType)
        ap.dSBox_freq_select_2.setProperty("value", 0.001)
        ap.verticalLayout_16.addWidget(ap.dSBox_freq_select_2)
        ap.gridLayout_5.addLayout(ap.verticalLayout_16, 1, 0, 1, 2)
        ap.horizontalLayout_5.addWidget(ap.groupBox_c2)
        ap.verticalLayout_global.addLayout(ap.horizontalLayout_5)
        ap.horizontalLayout_9 = QtWidgets.QHBoxLayout()
        ap.verticalLayout_modulation = QtWidgets.QVBoxLayout()
        # Modulación
        ap.groupBox_modulation =savingGroupBox(ap.tab, "group_modulation")
        ap.groupBox_modulation.setCheckable(True)
        ap.groupBox_modulation.setChecked(False)
        ap.gridLayout_3 = QtWidgets.QGridLayout(ap.groupBox_modulation)
        ap.pushButton_modulation_FM = QtWidgets.QPushButton(ap.groupBox_modulation)
        ap.pushButton_modulation_FM.setCheckable(True)
        ap.buttonGroup_Modulacion = savingButtonGroup(ap, "modulation_type")
        ap.buttonGroup_Modulacion.addButton(ap.pushButton_modulation_FM)
        ap.gridLayout_3.addWidget(ap.pushButton_modulation_FM, 2, 1, 1, 1)
        ap.pushButton_modulation_AM = QtWidgets.QPushButton(ap.groupBox_modulation)
        ap.pushButton_modulation_AM.setCheckable(True)
        ap.buttonGroup_Modulacion.addButton(ap.pushButton_modulation_AM)
        ap.gridLayout_3.addWidget(ap.pushButton_modulation_AM, 2, 0, 1, 1)
        ap.pushButton_modulation_ASK = QtWidgets.QPushButton(ap.groupBox_modulation)
        ap.pushButton_modulation_ASK.setCheckable(True)
        ap.buttonGroup_Modulacion.addButton(ap.pushButton_modulation_ASK)
        ap.gridLayout_3.addWidget(ap.pushButton_modulation_ASK, 0, 1, 1, 1)
        ap.groupBox = QtWidgets.QGroupBox(ap.groupBox_modulation)
        ap.gridLayout_11 = QtWidgets.QGridLayout(ap.groupBox)
        ap.pushButton_modulation_Manual = QtWidgets.QPushButton(ap.groupBox)
        ap.pushButton_modulation_Manual.setCheckable(True)
        ap.buttonGroup_OrigenModulacion = savingButtonGroup(ap, 'modulation_origin')
        ap.buttonGroup_OrigenModulacion.addButton(ap.pushButton_modulation_Manual)
        ap.gridLayout_11.addWidget(ap.pushButton_modulation_Manual, 1, 0, 1, 1)
        ap.pushButton_modulation_Ext_DC = QtWidgets.QPushButton(ap.groupBox)
        ap.pushButton_modulation_Ext_DC.setCheckable(True)
        ap.buttonGroup_OrigenModulacion.addButton(ap.pushButton_modulation_Ext_DC)
        ap.gridLayout_11.addWidget(ap.pushButton_modulation_Ext_DC, 1, 1, 1, 1)
        ap.pushButton_modulation_Channel2 = QtWidgets.QPushButton(ap.groupBox)
        ap.pushButton_modulation_Channel2.setCheckable(True)
        ap.buttonGroup_OrigenModulacion.addButton(ap.pushButton_modulation_Channel2)
        ap.gridLayout_11.addWidget(ap.pushButton_modulation_Channel2, 0, 0, 1, 1)
        ap.pushButton_modulation_Ext_AC = QtWidgets.QPushButton(ap.groupBox)
        ap.pushButton_modulation_Ext_AC.setCheckable(True)
        ap.buttonGroup_OrigenModulacion.addButton(ap.pushButton_modulation_Ext_AC)
        ap.gridLayout_11.addWidget(ap.pushButton_modulation_Ext_AC, 0, 1, 1, 1)
        ap.gridLayout_3.addWidget(ap.groupBox, 3, 0, 1, 3)
        ap.pushButton_modulation_PSK = QtWidgets.QPushButton(ap.groupBox_modulation)
        ap.pushButton_modulation_PSK.setCheckable(True)
        ap.buttonGroup_Modulacion.addButton(ap.pushButton_modulation_PSK)
        ap.gridLayout_3.addWidget(ap.pushButton_modulation_PSK, 0, 2, 1, 1)
        ap.pushButton_modulation_PM = QtWidgets.QPushButton(ap.groupBox_modulation)
        ap.pushButton_modulation_PM.setCheckable(True)
        ap.buttonGroup_Modulacion.addButton(ap.pushButton_modulation_PM)
        ap.gridLayout_3.addWidget(ap.pushButton_modulation_PM, 2, 2, 1, 1)
        ap.pushButton_modulation_FSK = QtWidgets.QPushButton(ap.groupBox_modulation)
        ap.pushButton_modulation_FSK.setCheckable(True)
        ap.buttonGroup_Modulacion.addButton(ap.pushButton_modulation_FSK)
        ap.gridLayout_3.addWidget(ap.pushButton_modulation_FSK, 0, 0, 1, 1)
        ap.pushButton_modulation_Burst = QtWidgets.QPushButton(ap.groupBox_modulation)
        ap.pushButton_modulation_Burst.setCheckable(True)
        ap.buttonGroup_Modulacion.addButton(ap.pushButton_modulation_Burst)
        ap.gridLayout_3.addWidget(ap.pushButton_modulation_Burst, 1, 1, 1, 1)
        ap.modulation_parameter = savingLineEdit(ap.groupBox_modulation, "modulation_param")
        ap.modulation_parameter.setMaxLength(127)
        ap.modulation_parameter.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        ap.gridLayout_3.addWidget(ap.modulation_parameter, 5, 0, 1, 3)
        ap.label_modulationParameter = QtWidgets.QLabel(ap.groupBox_modulation)
        # Sin este ajuste deja mucho espacio vertical alrededor de la etiqueta
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        ap.label_modulationParameter.setSizePolicy(sizePolicy)
        ap.gridLayout_3.addWidget(ap.label_modulationParameter, 4, 0, 1, 3)
        ap.verticalLayout_modulation.addWidget(ap.groupBox_modulation)
        ap.horizontalLayout_9.addLayout(ap.verticalLayout_modulation)
        ap.verticalLayout_9 = QtWidgets.QVBoxLayout()
        # Barrido
        ap.groupBox_sweep =savingGroupBox(ap.tab, "group_sweep")
        ap.groupBox_sweep.setCheckable(True)
        ap.groupBox_sweep.setChecked(False)
        ap.verticalLayout_43 = QtWidgets.QVBoxLayout(ap.groupBox_sweep)
        ap.horizontalLayout_12 = QtWidgets.QHBoxLayout()
        ap.pushButton_sweep_VCO = QtWidgets.QPushButton(ap.groupBox_sweep)
        ap.pushButton_sweep_VCO.setCheckable(True)
        ap.pushButton_sweep_VCO.setChecked(False)
        ap.pushButton_sweep_VCO.setDefault(False)
        ap.buttonGroup_Tipo_Barrido = savingButtonGroup(ap, 'sweep_type')
        ap.buttonGroup_Tipo_Barrido.addButton(ap.pushButton_sweep_VCO)
        ap.horizontalLayout_12.addWidget(ap.pushButton_sweep_VCO)
        ap.pushButton_sweep_Time = QtWidgets.QPushButton(ap.groupBox_sweep)
        ap.pushButton_sweep_Time.setCheckable(True)
        ap.pushButton_sweep_Time.setChecked(True)
        ap.buttonGroup_Tipo_Barrido.addButton(ap.pushButton_sweep_Time)
        ap.horizontalLayout_12.addWidget(ap.pushButton_sweep_Time)
        ap.verticalLayout_43.addLayout(ap.horizontalLayout_12)
        ap.horizontalLayout_13 = QtWidgets.QHBoxLayout()
        ap.pushButton_sweep_frequency = QtWidgets.QPushButton(ap.groupBox_sweep)
        ap.pushButton_sweep_frequency.setCheckable(True)
        ap.pushButton_sweep_frequency.setChecked(True)
        ap.pushButton_sweep_frequency.setDefault(False)
        ap.buttonGroup_Obj_Barrido = savingButtonGroup(ap, 'sweep_object')
        ap.buttonGroup_Obj_Barrido.addButton(ap.pushButton_sweep_frequency)
        ap.horizontalLayout_13.addWidget(ap.pushButton_sweep_frequency)
        ap.pushButton_sweep_amplitude = QtWidgets.QPushButton(ap.groupBox_sweep)
        ap.pushButton_sweep_amplitude.setCheckable(True)
        ap.buttonGroup_Obj_Barrido.addButton(ap.pushButton_sweep_amplitude)
        ap.horizontalLayout_13.addWidget(ap.pushButton_sweep_amplitude)
        ap.pushButton_sweep_offset =QtWidgets.QPushButton(ap.groupBox_sweep)
        ap.pushButton_sweep_offset.setCheckable(True)
        ap.buttonGroup_Obj_Barrido.addButton(ap.pushButton_sweep_offset)
        ap.horizontalLayout_13.addWidget(ap.pushButton_sweep_offset)
        ap.pushButton_sweep_duty =QtWidgets.QPushButton(ap.groupBox_sweep)
        ap.pushButton_sweep_duty.setCheckable(True)
        ap.buttonGroup_Obj_Barrido.addButton(ap.pushButton_sweep_duty)
        ap.horizontalLayout_13.addWidget(ap.pushButton_sweep_duty)
        ap.verticalLayout_43.addLayout(ap.horizontalLayout_13)
        ap.horizontalLayout_18 = QtWidgets.QHBoxLayout()
        ap.label = QtWidgets.QLabel(ap.groupBox_sweep)
        # Curioso
        # ap.label.setMaximumSize(QtCore.QSize(30, 16777215))
        ap.horizontalLayout_18.addWidget(ap.label)
        ap.dSBox_sweep_low = savingDoubleSpinBox(ap.groupBox_sweep, "sweep_low")
        ap.dSBox_sweep_low.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        ap.dSBox_sweep_low.setMaximum(9999999.0)
        ap.dSBox_sweep_low.setProperty("value", 10000.0)
        ap.horizontalLayout_18.addWidget(ap.dSBox_sweep_low)
        ap.verticalLayout_43.addLayout(ap.horizontalLayout_18)
        ap.horizontalLayout_17 = QtWidgets.QHBoxLayout()
        ap.label_2 = QtWidgets.QLabel(ap.groupBox_sweep)
        ap.label_2.setMaximumSize(QtCore.QSize(30, 16777215))
        ap.horizontalLayout_17.addWidget(ap.label_2)
        ap.dSBox_sweep_high = savingDoubleSpinBox(ap.groupBox_sweep, "sweep_high")
        ap.dSBox_sweep_high.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        ap.dSBox_sweep_high.setMaximum(9999999.0)
        ap.dSBox_sweep_high.setProperty("value", 20000.0)
        ap.horizontalLayout_17.addWidget(ap.dSBox_sweep_high)
        ap.verticalLayout_43.addLayout(ap.horizontalLayout_17)
        ap.horizontalLayout_16 = QtWidgets.QHBoxLayout()
        ap.label_3 = QtWidgets.QLabel(ap.groupBox_sweep)
        ap.label_3.setMaximumSize(QtCore.QSize(70, 16777215))
        ap.horizontalLayout_16.addWidget(ap.label_3)
        ap.dSBox_sweep_time = savingDoubleSpinBox(ap.groupBox_sweep, "sweep_time")
        ap.dSBox_sweep_time.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        ap.dSBox_sweep_time.setMaximum(999.99)
        ap.dSBox_sweep_time.setProperty("value", 2.0)
        ap.horizontalLayout_16.addWidget(ap.dSBox_sweep_time)
        ap.verticalLayout_43.addLayout(ap.horizontalLayout_16)
        ap.barrido_ok = QtWidgets.QHBoxLayout()
        ap.barrido_tipo = QtWidgets.QVBoxLayout()
        ap.horizontalLayout_14 = QtWidgets.QHBoxLayout()
        ap.pushButton_sweep_linear = QtWidgets.QPushButton(ap.groupBox_sweep)
        ap.pushButton_sweep_linear.setCheckable(True)
        ap.pushButton_sweep_linear.setChecked(True)
        ap.buttonGroup_Barrido_Lineal_log = savingButtonGroup(ap, 'linear_log_sweep')
        ap.buttonGroup_Barrido_Lineal_log.addButton(ap.pushButton_sweep_linear)
        ap.horizontalLayout_14.addWidget(ap.pushButton_sweep_linear)
        ap.pushButton_sweep_logaritmic = QtWidgets.QPushButton(ap.groupBox_sweep)
        ap.pushButton_sweep_logaritmic.setCheckable(True)
        ap.buttonGroup_Barrido_Lineal_log.addButton(ap.pushButton_sweep_logaritmic)
        ap.horizontalLayout_14.addWidget(ap.pushButton_sweep_logaritmic)
        ap.barrido_tipo.addLayout(ap.horizontalLayout_14)
        ap.horizontalLayout_15 = QtWidgets.QHBoxLayout()
        ap.pushButton_sweep_forth = QtWidgets.QPushButton(ap.groupBox_sweep)
        ap.pushButton_sweep_forth.setCheckable(True)
        ap.buttonGroup_Barrido_adelante_atras = savingButtonGroup(ap, 'back_forth_sweep')
        ap.buttonGroup_Barrido_adelante_atras.addButton(ap.pushButton_sweep_forth)
        ap.horizontalLayout_15.addWidget(ap.pushButton_sweep_forth)
        ap.pushButton_sweep_back = QtWidgets.QPushButton(ap.groupBox_sweep)
        ap.pushButton_sweep_back.setCheckable(True)
        ap.buttonGroup_Barrido_adelante_atras.addButton(ap.pushButton_sweep_back)
        ap.horizontalLayout_15.addWidget(ap.pushButton_sweep_back)
        ap.pushButton_sweep_back_forth = QtWidgets.QPushButton(ap.groupBox_sweep)
        ap.pushButton_sweep_back_forth.setCheckable(True)
        ap.buttonGroup_Barrido_adelante_atras.addButton(ap.pushButton_sweep_back_forth)
        ap.horizontalLayout_15.addWidget(ap.pushButton_sweep_back_forth)
        ap.barrido_tipo.addLayout(ap.horizontalLayout_15)
        ap.barrido_ok.addLayout(ap.barrido_tipo)
        ap.pushButton_sweep_start = savingPushButton(ap.groupBox_sweep, "sweep_start")
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        ap.pushButton_sweep_start.setSizePolicy(sizePolicy)
        ap.pushButton_sweep_start.setCheckable(True)
        ap.barrido_ok.addWidget(ap.pushButton_sweep_start)
        ap.verticalLayout_43.addLayout(ap.barrido_ok)
        ap.verticalLayout_9.addWidget(ap.groupBox_sweep)
        # Ajustes
        ap.groupBox1 = QtWidgets.QGroupBox(ap.tab)
        ap.horizontalLayout_6 = QtWidgets.QHBoxLayout(ap.groupBox1)
        ap.checkBox_beep = QtWidgets.QCheckBox(ap.groupBox1)
        ap.horizontalLayout_6.addWidget(ap.checkBox_beep)
        ap.comboBox_file_settings_selector = QtWidgets.QComboBox(ap.groupBox1)
        ap.comboBox_file_settings_selector.addItem("")
        ap.comboBox_file_settings_selector.addItem("")
        ap.horizontalLayout_6.addWidget(ap.comboBox_file_settings_selector)
        ap.pushButton_save_settings = QtWidgets.QPushButton(ap.groupBox1)
        ap.horizontalLayout_6.addWidget(ap.pushButton_save_settings)
        ap.pushButton_load_settings = QtWidgets.QPushButton(ap.groupBox1)
        ap.horizontalLayout_6.addWidget(ap.pushButton_load_settings)
        ap.verticalLayout_9.addWidget(ap.groupBox1)
        ap.horizontalLayout_9.addLayout(ap.verticalLayout_9)
        ap.verticalLayout_8 = QtWidgets.QVBoxLayout()
        # Medidas
        ap.groupBox_meter =savingGroupBox(ap.tab, "group_measure")
        ap.groupBox_meter.setCheckable(True)
        ap.groupBox_meter.setChecked(False)
        ap.horizontalLayout_3 = QtWidgets.QHBoxLayout(ap.groupBox_meter)
        ap.verticalLayout_7 = QtWidgets.QGridLayout()
        ap.lineEdit_Measurement_1 = savingLineEdit(ap.groupBox_meter, "measure_1")
        ap.lineEdit_Measurement_1.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        ap.verticalLayout_7.addWidget(ap.lineEdit_Measurement_1, 0, 1, 1, 1)
        ap.lineEdit_Measurement_5 = savingLineEdit(ap.groupBox_meter, "measure_2")
        ap.lineEdit_Measurement_5.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        ap.verticalLayout_7.addWidget(ap.lineEdit_Measurement_5, 4, 1, 1, 1)
        ap.lineEdit_Measurement_4 = savingLineEdit(ap.groupBox_meter, "measure_3")
        ap.lineEdit_Measurement_4.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        ap.verticalLayout_7.addWidget(ap.lineEdit_Measurement_4, 3, 1, 1, 1)
        ap.lineEdit_Measurement_2 = savingLineEdit(ap.groupBox_meter, "measure_4")
        ap.lineEdit_Measurement_2.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        ap.verticalLayout_7.addWidget(ap.lineEdit_Measurement_2, 1, 1, 1, 1)
        ap.lineEdit_Measurement_3 = savingLineEdit(ap.groupBox_meter, "measure_5")
        ap.lineEdit_Measurement_3.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        ap.verticalLayout_7.addWidget(ap.lineEdit_Measurement_3, 2, 1, 1, 1)
        ap.label_5 = QtWidgets.QLabel(ap.groupBox_meter)
        ap.verticalLayout_7.addWidget(ap.label_5, 0, 0, 1, 1)
        ap.label_6 = QtWidgets.QLabel(ap.groupBox_meter)
        ap.verticalLayout_7.addWidget(ap.label_6, 1, 0, 1, 1)
        ap.label_7 = QtWidgets.QLabel(ap.groupBox_meter)
        ap.verticalLayout_7.addWidget(ap.label_7, 2, 0, 1, 1)
        ap.label_8 = QtWidgets.QLabel(ap.groupBox_meter)
        ap.verticalLayout_7.addWidget(ap.label_8, 3, 0, 1, 1)
        ap.label_9 = QtWidgets.QLabel(ap.groupBox_meter)
        ap.verticalLayout_7.addWidget(ap.label_9, 4, 0, 1, 1)
        ap.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        ap.pushButton_measurement_1s = QtWidgets.QPushButton(ap.groupBox_meter)
        ap.pushButton_measurement_1s.setCheckable(True)
        ap.buttonGroup_Medida_intervalo = savingButtonGroup(ap, 'measuring_interval')
        ap.buttonGroup_Medida_intervalo.addButton(ap.pushButton_measurement_1s)
        ap.horizontalLayout_2.addWidget(ap.pushButton_measurement_1s)
        ap.pushButton_measurement_10s = QtWidgets.QPushButton(ap.groupBox_meter)
        ap.pushButton_measurement_10s.setCheckable(True)
        ap.buttonGroup_Medida_intervalo.addButton(ap.pushButton_measurement_10s)
        ap.horizontalLayout_2.addWidget(ap.pushButton_measurement_10s)
        ap.pushButton_measurement_100s = QtWidgets.QPushButton(ap.groupBox_meter)
        ap.pushButton_measurement_100s.setCheckable(True)
        ap.buttonGroup_Medida_intervalo.addButton(ap.pushButton_measurement_100s)
        ap.horizontalLayout_2.addWidget(ap.pushButton_measurement_100s)
        ap.verticalLayout_7.addLayout(ap.horizontalLayout_2, 5, 0, 1, 2)
        ap.horizontalLayout_3.addLayout(ap.verticalLayout_7)
        ap.groupBox2 = QtWidgets.QGroupBox(ap.groupBox_meter)
        ap.verticalLayout_6 = QtWidgets.QVBoxLayout(ap.groupBox2)
        ap.pushButton_Measurement_Freq = QtWidgets.QPushButton(ap.groupBox2)
        ap.pushButton_Measurement_Freq.setCheckable(True)
        ap.pushButton_Measurement_Freq.setChecked(True)
        ap.pushButton_Measurement_Freq.setDefault(False)
        ap.buttonGroup_Medida_cuenta_freq = savingButtonGroup(ap, 'frec_count_meas')
        ap.buttonGroup_Medida_cuenta_freq.addButton(ap.pushButton_Measurement_Freq)
        ap.verticalLayout_6.addWidget(ap.pushButton_Measurement_Freq)
        ap.pushButton_Measurement_Count = QtWidgets.QPushButton(ap.groupBox2)
        ap.pushButton_Measurement_Count.setCheckable(True)
        ap.buttonGroup_Medida_cuenta_freq.addButton(ap.pushButton_Measurement_Count)
        ap.verticalLayout_6.addWidget(ap.pushButton_Measurement_Count)
        ap.pushButton_Measurement_Zero = QtWidgets.QPushButton(ap.groupBox2)
        ap.verticalLayout_6.addWidget(ap.pushButton_Measurement_Zero)
        ap.pushButton_Measurement_Stop = QtWidgets.QPushButton(ap.groupBox2)
        ap.pushButton_Measurement_Stop.setCheckable(True)
        ap.verticalLayout_6.addWidget(ap.pushButton_Measurement_Stop)
        ap.comboBox_Measurement_Coupling = QtWidgets.QComboBox(ap.groupBox2)
        ap.comboBox_Measurement_Coupling.addItem("")
        ap.comboBox_Measurement_Coupling.addItem("")
        ap.verticalLayout_6.addWidget(ap.comboBox_Measurement_Coupling)
        ap.horizontalLayout_3.addWidget(ap.groupBox2)
        ap.verticalLayout_8.addWidget(ap.groupBox_meter)
        ap.horizontalLayout_8 = QtWidgets.QHBoxLayout()
        # Enlace
        ap.groupBox_uplink =savingGroupBox(ap.tab, "group_uplink")
        ap.groupBox_uplink.setCheckable(True)
        ap.groupBox_uplink.setChecked(False)
        ap.horizontalLayout_7 = QtWidgets.QHBoxLayout(ap.groupBox_uplink)
        ap.comboBox_uplink_select_role = QtWidgets.QComboBox(ap.groupBox_uplink)
        ap.comboBox_uplink_select_role.addItem("")
        ap.comboBox_uplink_select_role.addItem("")
        ap.horizontalLayout_7.addWidget(ap.comboBox_uplink_select_role)
        ap.horizontalLayout_8.addWidget(ap.groupBox_uplink)
        ap.verticalLayout_8.addLayout(ap.horizontalLayout_8)
        ap.horizontalLayout_9.addLayout(ap.verticalLayout_8)
        ap.verticalLayout_global.addLayout(ap.horizontalLayout_9)
        ap.tabWidget.addTab(ap.tab, "")
        # Solapa del gráfico
        ap.tab_2 = QtWidgets.QWidget()
        ap.verticalLayout_tab_2 = QtWidgets.QVBoxLayout(ap.tab_2)
        ap.verticalLayout_tab_2.setContentsMargins(0, 0, 0, 0)
        # Editor de ondas
        ap.chartview = savingWaveEdit(ap.waveData, 'waveedit')
        ap.chartview.setSizePolicy(QtWidgets.QSizePolicy(
                    QtWidgets.QSizePolicy.Expanding,
                    QtWidgets.QSizePolicy.Expanding))
        ap.verticalLayout_tab_2.addWidget(ap.chartview)
        # Formulario del gráfico
        ap.groupBox_2 = QtWidgets.QGroupBox(ap.tab_2)
        ap.formLayout_box2 = QtWidgets.QHBoxLayout(ap.groupBox_2)
        ap.groupBox_2.setTitle("")
        ap.pushButton_Ayuda = QtWidgets.QPushButton(ap.groupBox_2)
        ap.formLayout_box2.addWidget(ap.pushButton_Ayuda)
        ap.formLayout_box2.addWidget(ap.pushButton_Ayuda)
        ap.lineEdit_Expression = savingLineEdit(ap.groupBox_2, "wave_expresion")
        ap.formLayout_box2.addWidget(ap.lineEdit_Expression)
        ap.BtnExpApply = QtWidgets.QPushButton(ap.groupBox_2)
        ap.formLayout_box2.addWidget(ap.BtnExpApply)
        ap.pushButton_Enviar = QtWidgets.QPushButton(ap.groupBox_2)
        ap.formLayout_box2.addWidget(ap.pushButton_Enviar)
        ap.spinBox_wave_mem = savingSpinBox(ap.groupBox_2, "wave_save_mem")
        ap.spinBox_wave_mem.setMinimum(1)
        ap.spinBox_wave_mem.setMaximum(64)
        ap.formLayout_box2.addWidget(ap.spinBox_wave_mem)
        ap.pushButton_Exportar = QtWidgets.QPushButton(ap.groupBox_2)
        ap.formLayout_box2.addWidget(ap.pushButton_Exportar)
        ap.pushButton_Importar = QtWidgets.QPushButton(ap.groupBox_2)
        ap.formLayout_box2.addWidget(ap.pushButton_Importar)
        ap.verticalLayout_tab_2.addWidget(ap.groupBox_2)
        ap.tabWidget.addTab(ap.tab_2, "")
        ap.setCentralWidget(ap.tabWidget)
        # Estado y menu
        ap.statusbar = QtWidgets.QStatusBar(ap)
        ap.setStatusBar(ap.statusbar)
        ap.menubar = QtWidgets.QMenuBar(ap)
        ap.menuPuerto_serie = QtWidgets.QMenu(ap.menubar)
        ap.menuMenu = QtWidgets.QMenu(ap.menubar)
        ap.setMenuBar(ap.menubar)
        ap.actionAuto_detectar = QtWidgets.QAction(ap)
        ap.actionSalir = QtWidgets.QAction(ap)
        ap.menuPuerto_serie.addAction(ap.actionAuto_detectar)
        ap.menuMenu.addAction(ap.actionSalir)
        ap.menubar.addAction(ap.menuMenu.menuAction())
        ap.menubar.addAction(ap.menuPuerto_serie.menuAction())
        self.retranslateUi(ap)
        ap.tabWidget.setCurrentIndex(0)
        # QtCore.QMetaObject.connectSlotsByName(ap)

    def retranslateUi(self, ap):
        _translate = QtCore.QCoreApplication.translate
        ap.setWindowTitle(_translate("ap", "feelman. Control del sistema"))
        ap.groupBox_c1.setTitle(_translate("ap", "Canal 1"))
        ap.label_14.setText(_translate("ap", "Fase"))
        ap.dSBox_phase_1.setSuffix(_translate("ap", "º"))
        for i in range(len(waves1)):
            ap.comboBox_wave_select_1.setItemText(i, _translate("ap", waves1[i]))
        ap.label_12.setText(_translate("ap", "Amplitud"))
        ap.dSBox_amplitude_select_1.setSuffix(_translate("ap", "V"))
        ap.pushButton_apply_1.setText(_translate("ap", "Aplicar"))
        ap.label_15.setText(_translate("ap", "Ciclo útil"))
        ap.dSBox_duty_1.setSuffix(_translate("ap", "%"))
        ap.label_13.setText(_translate("ap", "Desplazamiento"))
        ap.dSBox_offset_1.setSuffix(_translate("ap", "V"))
        ap.label_11.setText(_translate("ap", "Frecuencia"))
        ap.dSBox_freq_select_1.setToolTip(_translate("ap", "Frequency"))
        ap.dSBox_freq_select_1.setSuffix(_translate("ap", "Hz"))
        ap.groupBox_c2.setTitle(_translate("ap", "Canal 2"))
        ap.label_27.setText(_translate("ap", "Amplitud"))
        ap.dSBox_amplitude_select_2.setSuffix(_translate("ap", "V"))
        ap.label_29.setText(_translate("ap", "Fase"))
        ap.dSBox_phase_2.setSuffix(_translate("ap", "º"))
        for i in range(len(waves2)):
            ap.comboBox_wave_select_2.setItemText(i, _translate("ap", waves2[i]))

        ap.pushButton_apply_2.setText(_translate("ap", "Aplicar"))
        ap.label_30.setText(_translate("ap", "Ciclo útil"))
        ap.dSBox_duty_2.setSuffix(_translate("ap", "%"))
        ap.label_28.setText(_translate("ap", "Desplazamiento"))
        ap.dSBox_offset_2.setSuffix(_translate("ap", "V"))
        ap.label_26.setText(_translate("ap", "Frecuencia"))
        ap.dSBox_freq_select_2.setToolTip(_translate("ap", "Frequency"))
        ap.dSBox_freq_select_2.setSuffix(_translate("ap", "Hz"))
        ap.groupBox_modulation.setTitle(_translate("ap", "Modulación"))
        ap.pushButton_modulation_FM.setText(_translate("ap", "FM"))
        ap.pushButton_modulation_AM.setText(_translate("ap", "AM"))
        ap.pushButton_modulation_ASK.setText(_translate("ap", "ASK"))
        ap.groupBox.setTitle(_translate("ap", "Origen"))
        ap.pushButton_modulation_Manual.setText(_translate("ap", "Manual"))
        ap.pushButton_modulation_Ext_DC.setText(_translate("ap", "Ext. (DC)"))
        ap.pushButton_modulation_Channel2.setText(_translate("ap", "Canal 2"))
        ap.pushButton_modulation_Ext_AC.setText(_translate("ap", "Ext. (AC)"))
        ap.pushButton_modulation_PSK.setText(_translate("ap", "PSK"))
        ap.pushButton_modulation_PM.setText(_translate("ap", "PM"))
        ap.pushButton_modulation_FSK.setText(_translate("ap", "FSK"))
        ap.pushButton_modulation_Burst.setText(_translate("ap", "Ráfaga"))
        ap.label_modulationParameter.setText(_translate("ap", "Parámetro:"))
        ap.groupBox_sweep.setTitle(_translate("ap", "Barrido"))
        ap.pushButton_sweep_VCO.setText(_translate("ap", "VCO"))
        ap.pushButton_sweep_Time.setText(_translate("ap", "Tiempo"))
        ap.pushButton_sweep_frequency.setText(_translate("ap", "Frec"))
        ap.pushButton_sweep_amplitude.setText(_translate("ap", "Ampl"))
        ap.pushButton_sweep_offset.setText(_translate("ap", "Desplaz."))
        ap.pushButton_sweep_duty.setText(_translate("ap", "C.Util"))
        ap.label.setText(_translate("ap", "De"))
        ap.label_2.setText(_translate("ap", "A"))
        ap.label_3.setText(_translate("ap", "Tiempo"))
        ap.dSBox_sweep_time.setSuffix(_translate("ap", " s."))
        ap.pushButton_sweep_linear.setText(_translate("ap", "Lineal"))
        ap.pushButton_sweep_logaritmic.setText(_translate("ap", "Logarítmico"))
        ap.pushButton_sweep_forth.setText(_translate("ap", "Adelante"))
        ap.pushButton_sweep_back.setText(_translate("ap", "Atrás"))
        ap.pushButton_sweep_back_forth.setText(_translate("ap", "Ad-atrás"))
        ap.pushButton_sweep_start.setText(_translate("ap", "Barrer"))
        ap.groupBox1.setTitle(_translate("ap", "Ajustes"))
        ap.checkBox_beep.setText(_translate("ap", "Bip"))
        ap.comboBox_file_settings_selector.setItemText(0, _translate("ap", "Por defecto"))
        ap.comboBox_file_settings_selector.setItemText(1, _translate("ap", "Nuevo..."))
        ap.pushButton_save_settings.setText(_translate("ap", "Guardar"))
        ap.pushButton_load_settings.setText(_translate("ap", "Cargar"))
        ap.groupBox_meter.setTitle(_translate("ap", "Medida"))
        ap.lineEdit_Measurement_4.setToolTip(_translate("ap", "Anchura pulso negativo"))
        ap.lineEdit_Measurement_3.setToolTip(_translate("ap", "Anchura pulso positivo"))
        ap.label_5.setText(_translate("ap", "N"))
        ap.label_6.setText(_translate("ap", "F"))
        ap.label_7.setText(_translate("ap", "+"))
        ap.label_8.setText(_translate("ap", "-"))
        ap.label_9.setText(_translate("ap", "%"))
        ap.pushButton_measurement_1s.setText(_translate("ap", "1s."))
        ap.pushButton_measurement_10s.setText(_translate("ap", "10s."))
        ap.pushButton_measurement_100s.setText(_translate("ap", "100s."))
        ap.pushButton_Measurement_Freq.setText(_translate("ap", "Frec"))
        ap.pushButton_Measurement_Count.setText(_translate("ap", "Cuenta"))
        ap.pushButton_Measurement_Zero.setText(_translate("ap", "Cero"))
        ap.pushButton_Measurement_Stop.setText(_translate("ap", "Parar"))
        ap.comboBox_Measurement_Coupling.setItemText(0, _translate("ap", "AC"))
        ap.comboBox_Measurement_Coupling.setItemText(1, _translate("ap", "DC"))
        ap.groupBox_uplink.setTitle(_translate("ap", "Enlace"))
        ap.comboBox_uplink_select_role.setItemText(0, _translate("ap", "Principal"))
        ap.comboBox_uplink_select_role.setItemText(1, _translate("ap", "Subordinado"))
        ap.tabWidget.setTabText(ap.tabWidget.indexOf(ap.tab), _translate("ap", "Control"))
        ap.pushButton_Enviar.setText(_translate("ap", "Enviar a Mem:"))
        ap.pushButton_Exportar.setText(_translate("ap", "Exportar..."))
        ap.pushButton_Importar.setText(_translate("ap", "Importar..."))
        ap.BtnExpApply.setText(_translate("ap", "Aplicar"))
        ap.lineEdit_Expression.setText(_translate("ap", "(sin(x)+y)/2"))
        ap.pushButton_Ayuda.setToolTip(_translate("ap", "Introduzca una expresión aritmética válida en python. Use <span style=\" font-weight:600;\">x</span> para el ángulo e <span style=\" font-weight:600;\">y</span> para el valor actual de la onda en el punto <span style=\" font-weight:600;\">x</span> antes de aplicar la función. El valor de <span style=\" font-weight:600;\">x</span> son ángulos centesimales, de 0 a 400 para el ciclo completo. Los valores de <span style=\" font-weight:600;\">y</span> fuera del rango -1...+1 se truncan."))
        ap.pushButton_Ayuda.setText(_translate("ap", "y =..."))
        ap.tabWidget.setTabText(ap.tabWidget.indexOf(ap.tab_2), _translate("ap", "Ondas"))
        ap.menuPuerto_serie.setTitle(_translate("ap", "Puerto"))
        ap.menuMenu.setTitle(_translate("ap", "Menu"))
        ap.actionAuto_detectar.setText(_translate("ap", "Auto detectar"))
        ap.actionSalir.setText(_translate("ap", "Salir"))
