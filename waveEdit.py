# This Python file uses the following encoding: utf-8
import math

""" Funciones de math para usarlas con eval() """
from math import (
    acos,
    acosh,
    asin,
    asinh,
    atan,
    atan2,
    atanh,
    ceil,
    comb,
    copysign,
    cos,
    cosh,
    degrees,
    dist,
    e,
    erf,
    erfc,
    exp,
    expm1,
    fabs,
    factorial,
    floor,
    fmod,
    frexp,
    fsum,
    gamma,
    gcd,
    hypot,
    inf,
    isclose,
    isfinite,
    isinf,
    isnan,
    isqrt,
    ldexp,
    lgamma,
    log,
    log10,
    log1p,
    log2,
    modf,
    nan,
    perm,
    pi,
    pow,
    radians,
    remainder,
    sin,
    sinh,
    sqrt,
    tan,
    tanh,
    tau,
    trunc,
)

import random
from PySide2 import QtCore
from PySide2.QtCharts import QtCharts
from PySide2.QtCore import (
    Qt,
    QPointF,
)


class editableChart(QtCharts.QChart):
    def __init__(self):
        super().__init__()


class waveEdit(QtCharts.QChartView):
    def __init__(self, data):
        super().__init__()
        self.editing = False
        self.clickPoint = None
        self.setContentsMargins(0, 0, 0, 0)
        self.data = data
        self.currentScale = 1.0
        self.series = QtCharts.QLineSeries()
        for i, y in enumerate(self.data):
            self.series.append(QPointF(i, y))
        self._chart = editableChart()
        axisY = QtCharts.QValueAxis(self._chart)
        axisY.setRange(-1, 1)
        self._chart.addSeries(self.series)
        # self._chart.setAxisY(axisY, self.series)
        self._chart.addAxis(axisY, Qt.AlignLeft)
        self.series.attachAxis(axisY)
        self._chart.legend().hide()
        self.setChart(self._chart)
        # self.setRubberBand(QtCharts.QChartView.RectangleRubberBand)
        # print(self._chart.axes()

    def maptoValue(self, point):
        return self._chart.mapToValue(
            self.mapToScene(QtCore.QPoint(point.x(), point.y()))
        )

    def replacePoints(self, point):
        """Cambia los puntos en la lista entre la posición anterior
        y la actual usando interpolación lineal"""
        ePoint = self.maptoValue(point)
        # bloquear señales de la serie, es algo más rápido que
        # inhibir actualizaciones
        self.series.blockSignals(True)
        # self.setUpdatesEnabled(False)
        ini = int(self.clickPoint.x())
        if ini < 0:
            ini = 0
        fin = int(ePoint.x())
        y = self.clickPoint.y()
        if fin < 0:
            fin = 0
            ePoint.setX(0.0)
        if ini == fin:
            return
        if ini > fin:
            ini, fin = fin, ini
            inc = (y - ePoint.y()) / (fin - ini)
        else:
            inc = (ePoint.y() - y) / (fin - ini)
        y = self.clickPoint.y()
        # desconectar la serie de la gráfica ?
        # no, es mejor bloquear señales
        # self._chart.removeSeries(self.series)
        for i in range(ini, fin):
            self.series.replace(i, float(i), y)
            y = y + inc
        # self._chart.addSeries(self.series)
        # list(map(self.series.attachAxis, self._chart.axes()))
        self.series.blockSignals(False)
        """ forzar señal de refresco reemplazando de nuevo el último
        punto (FIXME, debe haber otra forma más elegante) """
        self.series.replace(fin, float(fin), y)
        self.series.show()
        self.clickPoint = ePoint
        self.setUpdatesEnabled(True)
        self.update()

    def mousePressEvent(self, event):
        if event.button() == QtCore.Qt.MouseButton.LeftButton:
            self.editing = True
            self.clickPoint = self.maptoValue(event.localPos())

    def mouseMoveEvent(self, event):
        if self.editing:
            self.replacePoints(event.localPos())

    def mouseReleaseEvent(self, event):
        if event.button() == QtCore.Qt.MouseButton.LeftButton:
            self.editing = False

    def wheelEvent(self, event):
        step = 1.2
        if event.delta() > 0:
            if self.currentScale < 20:
                self.scale(step, step)
                self.currentScale = self.currentScale * step
        else:
            if self.currentScale > 1:
                self.scale(1 / step, 1 / step)
                self.currentScale = self.currentScale / step
        self.centerOn(event.position())

    def getDataFromGraph(self):
        self.getValuesFromGraph()
        return self.data

    def getValuesFromGraph(self):
        for x in range(0, self.series.count()):
            self.data[x] = self.series.at(x).y()

    def applyExpression(self, expr):
        self.getValuesFromGraph()
        nVals = len(self.data)
        self._chart.removeSeries(self.series)
        self.series = QtCharts.QLineSeries()
        for i, y in enumerate(self.data):
            # x se define para ser evaluada
            x = i * 2 * math.pi / nVals
            try:
                y = eval(expr)
            except (e):
                pass  # ¿ dejar como estaba ?
            self.data[i] = y
            self.series.append(QPointF(i, y))
        self._chart.addSeries(self.series)
        list(map(self.series.attachAxis, self._chart.axes()))

    def replaceData(self, data):
        """ FIXME: ¿ destruir la serie anterior ? """
        self.data = data
        self._chart.removeSeries(self.series)
        self.series = QtCharts.QLineSeries()
        for i, y in enumerate(self.data):
            self.series.append(QPointF(i, y))
        self._chart.addSeries(self.series)
        list(map(self.series.attachAxis, self._chart.axes()))


if __name__ == "__main__":
    import sys
    from PySide2.QtWidgets import (
        QApplication,
        QMainWindow,
    )

    size = 9000
    waveData = [0] * size
    app = QApplication(sys.argv)
    for i in range(size):
        waveData[i] = random.uniform(-0.5, 0.5)
    window = QMainWindow()
    window.setCentralWidget(waveEdit(waveData))
    window.show()
    window.resize(600, 400)
    sys.exit(app.exec_())
